package com.example.pcastillo.sugartest;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pcastillo on 5/29/17.
 */

@Table
public class ChildModel extends SugarRecord {

    public boolean booleanColumn;
    public byte byteColumn;
    public char charColumn;
    public short shortColumn;
    public int intColumn;
    public long longColumn;
    public float floatColumn;
    public double doubleColumn;

}
