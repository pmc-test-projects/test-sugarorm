package com.example.pcastillo.sugartest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.orm.SugarContext;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SugarContext.init(this);


        ChildModel child1 = new ChildModel();
        child1.booleanColumn = true;
        child1.byteColumn = 1;
        child1.charColumn = '1';
        child1.shortColumn = 1;
        child1.intColumn = 1;
        child1.longColumn = 1;
        child1.floatColumn = 1;
        child1.doubleColumn = 1.00;

        ChildModel child2 = new ChildModel();
        child2.booleanColumn = false;
        child2.byteColumn = 2;
        child2.charColumn = '2';
        child2.shortColumn = 2;
        child2.intColumn = 2;
        child2.longColumn = 2;
        child2.floatColumn = 2;
        child2.doubleColumn = 2.00;

        ChildModel child3 = new ChildModel();
        child3.booleanColumn = true;
        child3.byteColumn = 3;
        child3.charColumn = '3';
        child3.shortColumn = 3;
        child3.intColumn = 3;
        child3.longColumn = 3;
        child3.floatColumn = 3;
        child3.doubleColumn = 3.00;

        ChildModel child4 = new ChildModel();
        child4.booleanColumn = false;
        child4.byteColumn = 4;
        child4.charColumn = '4';
        child4.shortColumn = 4;
        child4.intColumn = 4;
        child4.longColumn = 4;
        child4.floatColumn = 4;
        child4.doubleColumn = 4.00;

        ChildModel child5 = new ChildModel();
        child5.booleanColumn = true;
        child5.byteColumn = 5;
        child5.charColumn = '5';
        child5.shortColumn = 5;
        child5.intColumn = 5;
        child5.longColumn = 5;
        child5.floatColumn = 5;
        child5.doubleColumn = 5.00;

        ArrayList<ChildModel> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        children.add(child4);
        children.add(child5);

        ParentModel parent = new ParentModel();

        parent.booleanColumn = false;
        parent.byteColumn = 99;
        parent.charColumn = '9';
        parent.shortColumn = 99;
        parent.intColumn = 99;
        parent.longColumn = 99;
        parent.floatColumn = 99;
        parent.doubleColumn = 99.00;

        parent.children = children;

        parent.save();
    }
}
